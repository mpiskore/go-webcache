package main

import (
	"fmt"
	"net/http"

	"example.com/webcache/webcache"
)

func main() {
	var repo webcache.Repository = webcache.PostgresRepoFromEnv()
	app := webcache.App{Repo: &repo}
	http.HandleFunc("/", app.CachedWebpageHandler)
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		fmt.Println("Server failed to start", err)
	}
}
