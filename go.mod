module example.com/webcache

go 1.16

require (
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.8 // indirect
)
