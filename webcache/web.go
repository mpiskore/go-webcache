package webcache

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/jackc/pgx/v4"
)

type Repository interface {
	GetCache(string) (string, error)
	SetCache(string) error
}

type PostgresRepo struct {
	Host, Port, User, Name string
}

func PostgresRepoFromEnv() *PostgresRepo {
	Host := os.Getenv("DBHOST")
	Port := os.Getenv("DBPORT")
	User := os.Getenv("DBUSER")
	Name := os.Getenv("DBNAME")
	return &PostgresRepo{Host: Host, Port: Port, User: User, Name: Name}
}

func (r *PostgresRepo) GetCache(ip string) (string, error) {
	DBString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable",
		r.Host, r.Port, r.User, r.Name)

	conn, err := pgx.Connect(context.Background(), DBString)
	if err != nil {
		return "", err
	}
	defer conn.Close(context.Background())
	var text string

	query := fmt.Sprintf("SELECT cached_content from cached_webpage where ip = '%s'", ip)
	err = conn.QueryRow(context.Background(), query).Scan(&text)
	if err != nil {
		return "", err
	}

	return text, nil
}

func (r *PostgresRepo) SetCache(ip string) error {
	DBString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable",
		r.Host, r.Port, r.User, r.Name)

	conn, err := pgx.Connect(context.Background(), DBString)
	if err != nil {
		return err
	}
	defer conn.Close(context.Background())

	created := time.Now()
	newCacheText := fmt.Sprintf("This is a cached webpage from %s", created.Format(time.ANSIC))
	_, err = conn.Exec(context.Background(), "INSERT INTO cached_webpage(cached_content, ip) values($1, $2)", newCacheText, ip)
	if err != nil {
		return err
	}
	return nil
}

type InMemoryRepo struct {
	Cache map[string]string
}

func (r *InMemoryRepo) GetCache(ip string) (string, error) {
	found := r.Cache[ip]
	if found == "" {
		return "", errors.New("No cache found")
	}
	return found, nil
}

func (r *InMemoryRepo) SetCache(ip string) error {
	created := time.Now()
	r.Cache[ip] = fmt.Sprintf("This is a cached webpage from %s", created.Format(time.ANSIC))
	return nil
}

type App struct {
	Repo *Repository
}

func (app *App) CachedWebpageHandler(w http.ResponseWriter, r *http.Request) {
	repo := *app.Repo
	cacheText, err := repo.GetCache(r.RemoteAddr)
	if err != nil {
		fmt.Fprintf(w, "No cache found")
		err = repo.SetCache(r.RemoteAddr)
		if err != nil {
			fmt.Println("ERROR: ", err)
		}
	} else {
		fmt.Fprintf(w, cacheText)
	}
}
