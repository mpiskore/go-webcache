package webcache

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNoCache(t *testing.T) {
	req, err := http.NewRequest("GET", "http://example.com/webcache", nil)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	var repo Repository = &InMemoryRepo{Cache: make(map[string]string)}
	app := App{Repo: &repo}

	app.CachedWebpageHandler(res, req)

	exp := "No cache found"
	act := res.Body.String()

	if exp != act {
		t.Fatalf("Expected >>%s<< got >>%s<<", exp, act)
	}
}

func TestCacheExists(t *testing.T) {
	req, err := http.NewRequest("GET", "http://example.com/webcache", nil)
	if err != nil {
		t.Fatal(err)
	}
	ip := "127.0.0.1:8001"
	req.RemoteAddr = ip
	res := httptest.NewRecorder()
	var repo Repository = &InMemoryRepo{Cache: make(map[string]string)}
	app := App{Repo: &repo}
	r := *app.Repo

	r.SetCache(ip)

	app.CachedWebpageHandler(res, req)

	exp := "This is a cached webpage from "
	if len(res.Body.String()) < 30 {
		t.Fatalf("Expected body to be at least 30 characters long, got %d", len(res.Body.String()))
	}
	act := res.Body.String()[:30]

	if exp != act {
		t.Fatalf("Expected >>%s<< got >>%s<<", exp, act)
	}
}
